# Test data for PERPL software (Pattern Extraction from Relative Positions of Localisations)

These are data files for use with the software at https://github.com/AlistairCurd/PERPL-Python3, version 0.2 at https://zenodo.org/record/8027437.

## Data sources

The original data for files starting *Nup107...* were localisations of Alexa Fluor 647 on SNAP-tagged Nup107 (C-terminus), acquired by Ulf Matti, Philipp Hoess and Jonas Ries at the European Molecular Biology Laboratory (EMBL), Heidelberg, Germany. The data was acquired as in Li, Y. et al., Real-time 3D single-molecule localization using experimental point spread functions. Nat. Methods 15(5), 367-369 (2018).

The original data for files starting *DNA-origami...* were localisations on DNA-origami structures, acquired by Thomas Schlichthaerle and Ralf Jungmann. They were acquired as in Schnitzbauer, J. et al., Super-resolution microscopy with DNA-PAINT. Nat. Protocols 12(6), 1198-1228 (2017).

The original data for files containing *ACTN2*, *LASP2* and *MYPN* were localisations of Z-disc proteins in adult rat cardiomyocytes. The localisations were of Alexa Fluor 647 on an Affimer bound to ACTN2 (*ACTN2-Affimer...*) or fluorescent protein constructs of alpha-actinin-2 (ACTN2), LASP2 and myopalladin (MYPN) with variants of Eos. Localisations were acquired using palm3d, as in York, A. et al., Confined activation and subdiffractive localization enables whole-cell PALM with genetically expressed probes. Nat. Methods 8(4), 327-333 (2011). (Standard 405 nm activation was used, as in Tiede, C. et al., Affimer proteins are versatile and renewable affinity reagents. eLife 6, e24903.) The relative position data for these localisations are provided here, with reference to the axial direction of the cardiomyocytes and the perpendicular to it (transverse). 

This data is made available under the Open Database License: http://opendatacommons.org/licenses/odbl/1.0/. Any rights in individual contents of the data files are licensed under the Database Contents License: http://opendatacommons.org/licenses/dbcl/1.0/. The data is licensed “as is” and without any warranty of any kind.

## Files included
  * *README.md*: This file, which contains information about the data in this project.
  * *license.md*: Data licensing file.
  
### Data files

#### *Nup107_SNAP_3D_GRROUPED.txt*:
  * All localisations acquired, without filtering, with all data fields included (listed in the first row).

#### *Nup107_SNAP_3D_GRROUPED_10nmZprec.txt*:
  * The localisations used in the analysis of Nup107 organisation in Fig. 1 of the submitted manuscript. There is one localisation per row. These are all of the localisations from the original field of view with estimated Z-precision of sigma < 10 nm. The columns in this list are x (nm), y (nm) and z (nm) of each localisation.
  * A suitable input file for *relative_positions.py*.

#### *Nup107_3D_10000_from_36297_locs.csv*:
  * The first 10,000 localisations from Nup107_SNAP_3D_GRROUPED_10nmZprec.txt. It can be used instead of the larger file if a quicker test (shorter then 5-10 minutes) is desired.
  * A suitable input file for *relative_positions.py*.

#### *Nup107_SNAP_3D_GRROUPED_10nmZprec_PERPL-relpos_200.0filter.csv*:
  * Pre-prepared results, generated by processing *Nup107_SNAP_3D_GRROUPED_10nmZprec.txt* with *relative_positions.py*, with a filter distance of 200 nm. This is a list of 3D relative positions among the localisations in *Nup107_SNAP_3D_GRROUPED_10nmZprec.txt*.
  * A suitable input file for *rot_2d_symm_fit.py*.

#### *DNA-origami_DNA-PAINT_locs_xyz.csv*
  * All localisations acquired, without filtering. One localisation per row with x, y, z coordinates in the three columns.

#### *DNA-origami_DNA-PAINT_locs_xyz_PERPL-relpos_250.0filter.csv*
  * Pre-prepared results, generated by processing *DNA-origami_DNA-PAINT_locs_xyz.csv* with *relative_positions.py*, with a filter distance of 250 nm. This is a list of 3D relative positions among the localisations in *DNA-origami_DNA-PAINT_locs_xyz.csv*.

#### *ACTN2-Affimer_locs_maxprec5.pkl*
  * Localisations acquired, after cropping the FOV to exclude repeated localisations of gold nanoparticles used for calibration. Also after filtering to include only localisations with estimated precision < 5nm.

#### *ACTN2-Affimer_PERPL-relpos_200.0filter_len2440488.pkl*
  * Relative positions between localisations with estimated precision < 5 nm. Relative positions were calculated using a filter_distance of 200 nm.
  * This is a *pandas* dataframe, containing the axial and transverse distances between localisations as explained in **Data sources** above. It can be read using ```pandas.read_pickle()```.

#### *ACTN2-mEos2_PERPL-relpos_200.0filter_6FOVs_aligned_len1229656.pkl*
  * Relative positions between localisations with estimated precision < 5 nm. Relative positions were calculated using a filter_distance of 200 nm.
  * This is a *pandas* dataframe, containing the axial and transverse distances between localisations as explained in **Data sources** above. It can be read using ```pandas.read_pickle()```.

#### *mEos3-LASP2_PERPL-relpos_200.0filter_5FOVs_aligned_len533140.pkl*
  * Relative positions between localisations with estimated precision < 5 nm. Relative positions were calculated using a filter_distance of 200 nm.
  * This is a *pandas* dataframe, containing the axial and transverse distances between localisations as explained in **Data sources** above. It can be read using ```pandas.read_pickle()```.

#### *mEos3-MYPN_NNS_aligned_5_FOVs_len1445698.pkl*
  * Relative positions between localisations with estimated precision < 5 nm. Relative positions were calculated using a filter_distance of 200 nm.
  * This is a *pandas* dataframe, containing the axial and transverse distances between localisations as explained in **Data sources** above. It can be read using ```pandas.read_pickle()```.
